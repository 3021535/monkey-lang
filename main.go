package main

import (
	"fmt"
	"monkey/repl"
	"os"
)

func main() {
	fmt.Println("Welcome to the Monkey Programming Language!")
	repl.Start(os.Stdin, os.Stdout)
}
