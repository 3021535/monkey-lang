An Golang implementation of the Monkey programming language (https://monkeylang.org/). Created by following The Interpreter Book (https://interpreterbook.com/).
